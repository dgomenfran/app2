using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace EssentialUIKit.Views.ECommerce
{
    /// <summary>
    /// The Delivery view. 
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HorariosView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveryView" /> class.
        /// </summary>
        public HorariosView()
        {
            this.InitializeComponent();
        }
    }
}