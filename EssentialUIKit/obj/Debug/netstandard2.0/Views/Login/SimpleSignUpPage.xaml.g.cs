//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Xamarin.Forms.Xaml.XamlResourceIdAttribute("EssentialUIKit.Views.Login.SimpleSignUpPage.xaml", "Views/Login/SimpleSignUpPage.xaml", typeof(global::EssentialUIKit.Views.Login.SimpleSignUpPage))]

namespace EssentialUIKit.Views.Login {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("Views\\Login\\SimpleSignUpPage.xaml")]
    public partial class SimpleSignUpPage : global::Xamarin.Forms.ContentPage {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::Xamarin.Forms.Label text;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::EssentialUIKit.Controls.BorderlessEntry NameEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::EssentialUIKit.Controls.BorderlessEntry PasswordEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::EssentialUIKit.Controls.BorderlessEntry ConfirmPasswordEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(SimpleSignUpPage));
            text = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::Xamarin.Forms.Label>(this, "text");
            NameEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::EssentialUIKit.Controls.BorderlessEntry>(this, "NameEntry");
            PasswordEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::EssentialUIKit.Controls.BorderlessEntry>(this, "PasswordEntry");
            ConfirmPasswordEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::EssentialUIKit.Controls.BorderlessEntry>(this, "ConfirmPasswordEntry");
        }
    }
}
