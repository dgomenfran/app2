//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Xamarin.Forms.Xaml.XamlResourceIdAttribute("EssentialUIKit.Views.ECommerce.CategoryTilePage.xaml", "Views/ECommerce/CategoryTilePage.xaml", typeof(global::EssentialUIKit.Views.ECommerce.CategoryTilePage))]

namespace EssentialUIKit.Views.ECommerce {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("Views\\ECommerce\\CategoryTilePage.xaml")]
    public partial class CategoryTilePage : global::Xamarin.Forms.ContentPage {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::Syncfusion.ListView.XForms.SfListView CategoryTile;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(CategoryTilePage));
            CategoryTile = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::Syncfusion.ListView.XForms.SfListView>(this, "CategoryTile");
        }
    }
}
